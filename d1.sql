-- CRUD

-- INSERT (CREATE)

-- to insert an antist in the artist table
INSERT INTO artists(name) VALUES ("Blackpink");
INSERT INTO artists(name) VALUES ("Rivermaya");

-- to insert albums in the albums table:
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("The Album", "2020-10-2", 1);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Trip", "1996-01-01", 2);

-- to insert songs in the songs table:
INSERT INTO songs (song_name, length, genre, album_id) VALUES ('Ice Cream', '00:04:16', 'Kpop', 1);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ('You Never Know', '00:03:59', 'kpop', 1);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ('Kundiman', '00:03:54', 'OPM', 2); 
INSERT INTO songs (song_name, length, genre, album_id) VALUES ('Kisapmata', '00:04:39', 'OPM', 2);


--SELECT (READ)

-- DIsplay the title and genre of all the songs.
SELECT song_name, genre FROM songs;

-- Display all the fields frin songs table.
SELECT * FROM songs;

-- Display the song name with OPM genre only
SELECT song_name FROM songs WHERE genre = "OPM";

-- Display the title and lenth of the K-pop songs that are more than 4mins
SELECT song_name, length FROM songs WHERE length > 400 and genre = "Kpop";


--UPDATE (UPDATE)

-- update the length of you never know to 00:04:00;
-- Update <table_name> SET <field_name> = <value? WHERE <validation_field> = <Value>;
UPDATE songs SET length = 400 WHERE song_name = "You Never Know";



--DELETE (DELETE)
-- delete all k-pop songs that are more than 4:00 mins
DELETE FROM songs WHERE genre = "Kpop" AND length > 400;

-- Removing the WHERE clause will delete all rrows.
DELETE FROM songs;


INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Kisapmata", SEC_TO_TIME(279), "OPM", 2);

SEC_TO_TIME po para maconvert yung seconds to time pag nag insert na data sa db

