
--add

INSERT INTO users (email, password, dateTime_created) VALUES ("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00");
INSERT INTO users (email, password, dateTime_created) VALUES ("juandelacruz@gmail.com", "passwordB", "2021-01-01 02:00:00");
INSERT INTO users (email, password, dateTime_created) VALUES ("janesmith@gmail.com", "passwordC", "2021-01-01 03:00:00");
INSERT INTO users (email, password, dateTime_created) VALUES ("mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00");
INSERT INTO users (email, password, dateTime_created) VALUES ("johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");


-- post
INSERT INTO posts (author_id, title, content, dateTime_posted) VALUES (1, "First Code", "Hello World!", "2021-01-02 01:00:00");
INSERT INTO posts (author_id, title, content, dateTime_posted) VALUES (1, "Second Code", "Hello Earth!", "2021-01-02 02:00:00");
INSERT INTO posts (author_id, title, content, dateTime_posted) VALUES (2, "Third Code", "Welcome to Mars!", "2021-01-02 03:00:00");
INSERT INTO posts (author_id, title, content, dateTime_posted) VALUES (4, "Fourth Code", "Bye bue solar system!", "2021-01-02 04:00:00");


-- get all posts with an authord ID of 1
SELECT * FROM posts WHERE author_id = 1;

-- get all the user's email and datetime of creation
SELECT email, dateTime_created FROM users;

--Update a post's content from "Hello earth" to "Hello to the people of the earth" where is initial content is "Hello Earth! by using the records ID."
UPDATE posts SET content = "Hello to the people of the Earth!" where id = 2;


-- delete johndoe@gmail.com
DELETE FROM users WHERE email = "johndoe@gmail.com";